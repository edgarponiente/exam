@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">

        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{!! isset($edit) ? 'Edit ' : 'Add ' !!}Post</div>

                <div class="panel-body">

                <form class="form-horizontal" method="POST" action="{!! (isset($edit) ? route('post.update', [$edit->id]) : route('post.store')) !!}">
                    {!! csrf_field() !!}
                    {!! (isset($edit) ? method_field('PUT') : method_field('POST')) !!}
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="post">{!! (isset($edit) ? $edit->post : old('post')) !!}</textarea>
                    </div>
                  </div>
                  
                  
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                  </div>
                </form>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
