@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">

        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Posts</div>

                <div class="panel-body">

                <div class="form-group">
                    <a href="{!! route('post.create') !!}" class="btn btn-success">Add New Post</a>
                </div>

                    <table class="table table-bordered">
                        <tr>
                            <th>Description</th>
                            <th>Posted By:</th>
                            <th>Created At: </th>
                            <th>Updated on: </th>
                            <th></th>
                        </tr>
                    @foreach( $posts as $post )
                        <tr>
                            <td>{!! $post->post !!}</td>
                            <td>{!! $post->user ? $post->user->username : '' !!}</td>
                            <td>{!! $post->created_at !!}</td>
                            <td>{!! $post->updated_at !!}</td>
                            <td>
                                <a class="btn-success btn btn-xs" href="{!! route('post.edit', [$post->id]) !!}">Edit</a>
                                <form method="POST" action="{!! route('post.destroy', [$post->id]) !!}" style="display: inline-block;">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <input type="submit" class="btn btn-xs btn-danger" value="Delete" />
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
