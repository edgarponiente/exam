<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testLoginMustBeCorrect()
    {
    	$this->visit('/login')
    		->type('edgar', 'username')
    		->type('123456', 'password')
    		->press('Login')
    		->seePageIs('/post');
    }
}
